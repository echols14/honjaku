package ss.virovitica.honjaku.di

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ss.virovitica.honjaku.net.MicrosoftClient
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetModule {
    companion object {
        private const val MICROSOFT_OK_CLIENT = "microsoftOkClient"
        const val MICROSOFT_RETROFIT = "microsoftRetrofit"
    }

    @Provides @Singleton
    internal fun provideInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    //Microsoft

    @Provides @Singleton @Named(MICROSOFT_OK_CLIENT)
    internal fun provideMicrosoftOkClient(interceptor: HttpLoggingInterceptor): OkHttpClient =
        OkHttpClient.Builder().addInterceptor(interceptor)
            .readTimeout(MicrosoftClient.TIMEOUT_SECONDS, TimeUnit.SECONDS).connectTimeout(MicrosoftClient.TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .build()

    @Provides @Singleton @Named(MICROSOFT_RETROFIT)
    internal fun provideMicrosoftRetrofit(@Named(MICROSOFT_OK_CLIENT) okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(MicrosoftClient.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create()).build() //uses Gson to interpret the response
}