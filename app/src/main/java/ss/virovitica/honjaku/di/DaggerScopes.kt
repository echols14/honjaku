package ss.virovitica.honjaku.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
internal annotation class PerActivity

@Scope
@Retention(AnnotationRetention.RUNTIME)
internal annotation class PerFragment