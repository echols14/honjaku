package ss.virovitica.honjaku.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ss.virovitica.honjaku.activity.main.di.MainModule
import ss.virovitica.honjaku.activity.main.view.MainActivity
import ss.virovitica.honjaku.activity.save.di.SaveModule
import ss.virovitica.honjaku.activity.save.view.SaveActivity
import ss.virovitica.honjaku.activity.sentence.di.SentenceModule
import ss.virovitica.honjaku.activity.sentence.view.SentenceActivity
import ss.virovitica.honjaku.activity.singleWord.di.SingleWordModule
import ss.virovitica.honjaku.activity.singleWord.view.SingleWordActivity
import ss.virovitica.honjaku.activity.word.di.WordModule
import ss.virovitica.honjaku.activity.word.view.WordActivity

@Module
abstract class ActivityModule {
    @PerActivity
    @ContributesAndroidInjector(modules = [MainModule::class])
    internal abstract fun contributeMainActivity(): MainActivity
    @PerActivity
    @ContributesAndroidInjector(modules = [SentenceModule::class])
    internal abstract fun contributeSentenceActivity(): SentenceActivity
    @PerActivity
    @ContributesAndroidInjector(modules = [WordModule::class])
    internal abstract fun contributeWordActivity(): WordActivity
    @PerActivity
    @ContributesAndroidInjector(modules = [SaveModule::class])
    internal abstract fun contributeSaveActivity(): SaveActivity
    @PerActivity
    @ContributesAndroidInjector(modules = [SingleWordModule::class])
    internal abstract fun contributeSingleWordActivity(): SingleWordActivity
}