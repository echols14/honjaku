package ss.virovitica.honjaku.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ss.virovitica.honjaku.fragment.WordFragment
import ss.virovitica.honjaku.fragment.WordFragmentModule

@Module
abstract class FragmentModule {
    @PerFragment
    @ContributesAndroidInjector(modules = [WordFragmentModule::class])
    internal abstract fun contributeArticleFragment(): WordFragment
}
