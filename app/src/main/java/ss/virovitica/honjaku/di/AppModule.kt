package ss.virovitica.honjaku.di

import dagger.Module
import dagger.Provides
import ss.virovitica.honjaku.HonjakuApp
import ss.virovitica.honjaku.activity.main.di.MainModule
import ss.virovitica.honjaku.activity.save.di.SaveModule
import ss.virovitica.honjaku.activity.sentence.di.SentenceModule
import ss.virovitica.honjaku.activity.singleWord.di.SingleWordModule
import ss.virovitica.honjaku.activity.word.di.WordModule
import ss.virovitica.honjaku.interactor.*
import javax.inject.Singleton

@Module
class AppModule(private val app: HonjakuApp) {
    @Provides @Singleton
    internal fun provideNewsApp(): HonjakuApp = app
    @Provides @Singleton
    internal fun provideMainModule(): MainModule = MainModule()
    @Provides @Singleton
    internal fun provideSentenceModule(): SentenceModule = SentenceModule()
    @Provides @Singleton
    internal fun provideWordModule(): WordModule = WordModule()
    @Provides @Singleton
    internal fun provideSaveModule(): SaveModule = SaveModule()
    @Provides @Singleton
    internal fun provideSingleWordModule(): SingleWordModule = SingleWordModule()
    @Provides @Singleton
    fun provideInteractor(interactor: MicrosoftInteractor): ITranslationInteractor = interactor
}