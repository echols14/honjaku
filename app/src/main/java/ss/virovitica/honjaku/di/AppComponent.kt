package ss.virovitica.honjaku.di

import dagger.Component
import dagger.android.AndroidInjectionModule
import ss.virovitica.honjaku.HonjakuApp
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetModule::class, AndroidInjectionModule::class, ActivityModule::class, FragmentModule::class])
interface AppComponent {
    fun inject(app: HonjakuApp)
}