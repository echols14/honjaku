package ss.virovitica.honjaku.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.view_word_summary.view.*
import org.jetbrains.anko.startActivity
import ss.virovitica.honjaku.R
import ss.virovitica.honjaku.activity.word.view.WordActivity
import ss.virovitica.honjaku.activity.word.view.WordActivity.Companion.FROM_SAVED_SENTENCE_KEY
import ss.virovitica.honjaku.activity.word.view.WordActivity.Companion.SENTENCE_ID_KEY
import ss.virovitica.honjaku.activity.word.view.WordActivity.Companion.WORD_INDEX_KEY
import ss.virovitica.honjaku.di.PerActivity
import ss.virovitica.honjaku.model.SentenceTranslation
import ss.virovitica.honjaku.model.WordTranslation
import javax.inject.Inject

@PerActivity
class SentenceRecyclerAdapter @Inject constructor(): RecyclerView.Adapter<SentenceRecyclerAdapter.WordHolder>() {
    //members
    var sentence: SentenceTranslation = SentenceTranslation()
        set(value) {
            field = value
            this.notifyDataSetChanged()
        }
    var fromSaved = false

    //functions

    override fun onCreateViewHolder(parent: ViewGroup, index: Int): WordHolder =
        WordHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_word_summary, parent, false))
    override fun getItemCount(): Int = sentence.size
    override fun onBindViewHolder(holder: WordHolder, index: Int) = holder.bind(sentence.id, index, sentence[index] ?: WordTranslation(), fromSaved)

    //ViewHolder child class
    class WordHolder(parent: View) : RecyclerView.ViewHolder(parent), View.OnClickListener {
        //members
        private lateinit var context: Context
        private var sentenceID = ""
        private var index: Int = -1
        private var fromSavedSentence = false
        private var id = ""

        //constructor
        init { parent.setOnClickListener(this) }

        //functions

        internal fun bind(sentenceIdIn: String, indexIn: Int, wordTranslation: WordTranslation, fromSaved: Boolean) {
            sentenceID = sentenceIdIn
            index = indexIn
            fromSavedSentence = fromSaved
            context = itemView.context
            id = wordTranslation.id
            itemView.headerText.text = itemView.context.getString(R.string.numbered_text_holder, indexIn+1, wordTranslation.displaySource)
            var summary = wordTranslation.prettyTranslationSummary()
            if(summary.isEmpty()) {
                summary = context.getString(R.string.no_translation)
            }
            itemView.summaryText.text = summary
        }

        override fun onClick(view: View) {
            itemView.context.startActivity<WordActivity>(SENTENCE_ID_KEY to sentenceID,
                WORD_INDEX_KEY to index, FROM_SAVED_SENTENCE_KEY to fromSavedSentence)
        }
    }
}