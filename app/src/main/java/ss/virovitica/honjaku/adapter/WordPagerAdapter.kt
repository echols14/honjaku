package ss.virovitica.honjaku.adapter

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import ss.virovitica.honjaku.fragment.WordFragment
import ss.virovitica.honjaku.model.WordTranslation

class WordPagerAdapter(fm: FragmentManager): FragmentStatePagerAdapter(fm) {
    internal var words = arrayOf<WordTranslation>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    override fun getItem(position: Int) = WordFragment.newInstance(words[position])
    override fun getCount(): Int = words.size
}