package ss.virovitica.honjaku.adapter

import android.content.Context
import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.jetbrains.anko.startActivity
import ss.virovitica.honjaku.R
import ss.virovitica.honjaku.model.SentenceTranslation
import ss.virovitica.honjaku.model.WordTranslation
import kotlinx.android.synthetic.main.view_section_header.view.*
import kotlinx.android.synthetic.main.view_sentence_summary.view.*
import kotlinx.android.synthetic.main.view_sentence_summary.view.deleteButton
import kotlinx.android.synthetic.main.view_word_summary.view.headerText
import kotlinx.android.synthetic.main.view_word_summary.view.summaryText
import ss.virovitica.honjaku.activity.save.presenter.ISavePresenter
import ss.virovitica.honjaku.activity.sentence.view.SentenceActivity
import ss.virovitica.honjaku.activity.sentence.view.SentenceActivity.Companion.SENTENCE_SAVED_BOOL_KEY
import ss.virovitica.honjaku.activity.singleWord.view.SingleWordActivity

class SaveRecyclerAdapter(private val context: Context, private val presenter: ISavePresenter,
                          private val inflater: LayoutInflater): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    //members
    private var wrapperList: ArrayList<AdapterWrapper> = ArrayList()

    //functions

    fun loadData(list : List<AdapterWrapper>) {
        wrapperList.clear()
        wrapperList.addAll(list)
        notifyDataSetChanged()
    }

    //***RecyclerView.Adapter***//

    override fun getItemViewType(index: Int) = wrapperList[index].layoutId
    override fun onCreateViewHolder(parent: ViewGroup, @LayoutRes layoutID: Int): RecyclerView.ViewHolder {
        val view = inflater.inflate(layoutID, parent, false)
        return when(layoutID){
            R.layout.view_section_header -> TextViewHolder(view) //section title
            R.layout.view_sentence_summary -> SentenceViewHolder(view) //sentence
            else -> WordViewHolder(view) //word
        }
    }
    override fun getItemCount() = wrapperList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, index: Int) {
        when (holder) {
            is TextViewHolder -> {
                val message = wrapperList[index].data as String
                holder.bind(message)
            }
            is SentenceViewHolder -> {
                val sentence = wrapperList[index].data as SentenceTranslation
                holder.bind(sentence, index)
            }
            is WordViewHolder -> {
                val word = wrapperList[index].data as WordTranslation
                holder.bind(word, index)
            }
            else -> Log.e("SaveRecyclerAdapter", "Unknown ViewHolder type")
        }
    }

    //ViewHolder child classes

    internal inner class TextViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        internal fun bind(message: String) {
            itemView.textView.text = message
        }
    }

    internal inner class SentenceViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener {
        init { itemView.setOnClickListener(this) }
        private lateinit var sentence: SentenceTranslation
        private var index = -1
        internal fun bind(sentenceIn: SentenceTranslation, indexIn: Int) {
            sentence = sentenceIn
            index = indexIn
            itemView.originalSentence.text = sentenceIn.printOriginal()
            itemView.translatedSentence.text = sentenceIn.printSimple()
            itemView.deleteButton.setOnClickListener {
                presenter.deleteSentence(sentence.id)
            }
        }
        override fun onClick(v: View) {
            v.context.startActivity<SentenceActivity>(
                SentenceActivity.SENTENCE_ID_KEY to sentence.id, SENTENCE_SAVED_BOOL_KEY to true)
        }
    }

    internal inner class WordViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener {
        init { itemView.setOnClickListener(this) }
        private lateinit var word: WordTranslation
        private var index = -1
        internal fun bind(wordIn: WordTranslation, indexIn: Int) {
            word = wordIn
            index = indexIn
            itemView.headerText.text = context.getString(R.string.generic_summary_header, word.sourceLanguage, word.displaySource)
            var summary = word.prettyTranslationSummary()
            if(summary.isEmpty()) {
                summary = context.getString(R.string.no_translation)
            }
            itemView.summaryText.text = itemView.context.getString(R.string.generic_summary_header, word.targetLanguage, summary)
            itemView.deleteButton.setOnClickListener {
                presenter.deleteWord(word.id)
            }
        }
        override fun onClick(v: View) {
            v.context.startActivity<SingleWordActivity>(SingleWordActivity.KEY_WORD_ID to word.id)
        }
    }

    class AdapterWrapper(@LayoutRes val layoutId: Int, val data: Any)
}
