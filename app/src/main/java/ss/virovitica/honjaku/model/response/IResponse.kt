package ss.virovitica.honjaku.model.response

interface IResponse {
    override fun toString(): String
}