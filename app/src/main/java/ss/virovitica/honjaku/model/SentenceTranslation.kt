package ss.virovitica.honjaku.model

import io.realm.RealmList
import io.realm.RealmObject
import java.lang.StringBuilder

open class SentenceTranslation(): RealmObject() {
    companion object {
        const val FIELD_IS_CURRENT = "isCurrent"
        const val FIELD_ID = "id"
    }

    //members
    val size
        get() = words.size
    operator fun get(index: Int) = words[index]

    //members
    lateinit var words: RealmList<WordTranslation>
    lateinit var id: String
        private set
    var isCurrent: Boolean = false
    var sourceLanguage = ""
        set(value) {
            field = value
            for(word in words) {
                word.sourceLanguage = value
            }
        }
    var targetLanguage= ""
        set(value) {
            field = value
            for(word in words) {
                word.targetLanguage = value
            }
        }

    //constructor
    constructor(idIn: String): this() {
        id = idIn
    }

    //functions

    fun toWordArray(): Array<WordTranslation> = words.toTypedArray()

    fun printSimple(): String {
        val builder = StringBuilder()
        for((i, word) in words.withIndex()) {
            var mostProbable: String? = null
            var mostProbableVal = 0F
            for(translation in word.translations) {
                if(translation.confidence > mostProbableVal) {
                    mostProbableVal = translation.confidence
                    mostProbable = translation.displayTarget
                }
            }
            if(mostProbable != null) {
                builder.append(mostProbable)
                if(i < words.size - 1) builder.append(", ")
            }
        }
        return builder.toString()
    }

    fun printOriginal(): String {
        val builder = StringBuilder("$sourceLanguage: ")
        for ((i, word) in words.withIndex()) {
            builder.append(word.displaySource)
            if(i < words.size - 1) builder.append(" ")
        }
        return builder.toString()
    }
}