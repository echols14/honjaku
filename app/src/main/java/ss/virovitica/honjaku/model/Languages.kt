package ss.virovitica.honjaku.model

//If Languages.kt is changed, please also update values/strings.xml(languagesArray)
class Languages {
    companion object {
        private val LANGUAGE_CODES = arrayOf("en", "af", "ar", "bn", "bs", "bg", "ca", "zh-Hans", "hr", "cs", "da",
                "nl", "et", "fi", "fr", "de", "el", "ht", "he", "hi", "mww", "hu", "is", "id", "it", "ja", "sw", "tlh",
                "ko", "lv", "lt", "ms", "mt", "nb", "fa", "pl", "pt", "ro", "ru", "sr-Latn", "sk", "sl", "es", "sv", "ta",
                "th", "tr", "uk", "ur", "vi", "cy")

        //functions

        fun spinnerIndexToLangCodeMicrosoft(index: Int) =
            if(index in 0 until LANGUAGE_CODES.size) LANGUAGE_CODES[index]
            else LANGUAGE_CODES[0]
    }
}