package ss.virovitica.honjaku.model.response.microsoft

import io.realm.RealmList
import ss.virovitica.honjaku.model.SentenceTranslation
import ss.virovitica.honjaku.model.WordTranslation
import java.util.*

class MicrosoftResponseMapper {
    companion object {
        fun responseToModel(response: MicrosoftResponse): SentenceTranslation {
            val list = RealmList<WordTranslation>()
            response.forEach {
                list.add(WordTranslation(it))
            }
            val sentence = SentenceTranslation(UUID.randomUUID().toString())
            sentence.words = list
            return sentence
        }
    }
}