package ss.virovitica.honjaku.model

import android.os.Parcel
import android.os.Parcelable
import io.realm.RealmList
import io.realm.RealmObject
import ss.virovitica.honjaku.model.response.microsoft.ResponseUnit
import ss.virovitica.honjaku.model.response.microsoft.TranslationUnit
import java.lang.StringBuilder
import java.util.*

open class WordTranslation(): RealmObject(), Parcelable {
    companion object {
        @JvmField
        internal val CREATOR: Parcelable.Creator<WordTranslation> = object: Parcelable.Creator<WordTranslation> { //for Parcelable
            override fun createFromParcel(parcel: Parcel): WordTranslation = WordTranslation(parcel)
            override fun newArray(size: Int): Array<WordTranslation?> = arrayOfNulls(size)
        }

        const val FIELD_IS_CURRENT = "isCurrent"
        const val FIELD_IN_SENTENCE = "inSentence"
        const val FIELD_ID = "id"
    }

    //members
    var id: String = UUID.randomUUID().toString()
        private set
    var normalizedSource: String? = null
        private set
    var displaySource: String? = null
        private set
    var translations: RealmList<TranslationUnit> = RealmList()
        private set(value) {
            Collections.sort(value, compareBy {it.confidence})
            field = value
        }
    var isCurrent: Boolean = false
    var inSentence: Boolean = true
    var sourceLanguage = "NULL"
    var targetLanguage = "NULL"

    //constructor
    constructor(responseUnit: ResponseUnit): this() {
        normalizedSource = responseUnit.normalizedSource
        displaySource = responseUnit.displaySource
        translations = RealmList()
        responseUnit.translations.toCollection(translations)
    }

    //functions

    fun prettyTranslationSummary(): String {
        val builder = StringBuilder()
        translations.forEachIndexed { i, translationUnit ->
            builder.append(i + 1)
            builder.append(". ")
            builder.append(translationUnit.displayTarget)
            if (i < translations.size - 1) builder.append(", ")
        }
        return builder.toString()
    }

    fun prettyTranslationDetail(): String {
        val builder = StringBuilder("$targetLanguage:\n\n")
        translations.forEachIndexed { i, translationUnit ->
            builder.append(i + 1)
            builder.append(". ")
            if(translationUnit.prefixWord != null && translationUnit.prefixWord != "") {
                builder.append('[')
                builder.append(translationUnit.prefixWord)
                builder.append("] ")
            }
            builder.append(translationUnit.displayTarget)
            builder.append(" (")
            builder.append(translationUnit.posTag)
            builder.append(')')
            if(i < translations.size - 1 || translationUnit.backTranslations.size != 0) builder.append('\n')
            translationUnit.backTranslations.forEachIndexed{ j, backTranslation ->
                builder.append(backTranslation.displayText)
                if(j < translationUnit.backTranslations.size - 1) builder.append(", ")
            }
            if (i < translations.size - 1) builder.append("\n\n")
        }
        return builder.toString()
    }

    //***Parcelable***//

    protected constructor(parcel: Parcel): this() {
        id = parcel.readString() ?: "NULL"
        normalizedSource = parcel.readString()
        displaySource = parcel.readString()
        parcel.readTypedList(translations, TranslationUnit.CREATOR)
        isCurrent = (parcel.readInt() != 0)
        sourceLanguage = parcel.readString() ?: "NULL"
        targetLanguage = parcel.readString() ?: "NULL"
    }
    override fun describeContents() = 0
    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(id)
        dest.writeString(normalizedSource)
        dest.writeString(displaySource)
        dest.writeTypedList(translations)
        dest.writeInt(if(isCurrent) 1 else 0)
        dest.writeString(sourceLanguage)
        dest.writeString(targetLanguage)
    }
}