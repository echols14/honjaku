package ss.virovitica.honjaku.model.response.microsoft

import android.os.Parcel
import android.os.Parcelable
import io.realm.RealmList
import io.realm.RealmObject
import ss.virovitica.honjaku.model.response.IResponse

class MicrosoftResponse: ArrayList<ResponseUnit>(), IResponse
data class ResponseUnit(val normalizedSource: String, val displaySource: String, val translations: Array<TranslationUnit>) {
    override fun equals(other: Any?): Boolean { //generated code
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as ResponseUnit
        if (normalizedSource != other.normalizedSource) return false
        if (displaySource != other.displaySource) return false
        if (!translations.contentEquals(other.translations)) return false
        return true
    }
    override fun hashCode(): Int { //generated code
        var result = normalizedSource.hashCode()
        result = 31 * result + displaySource.hashCode()
        result = 31 * result + translations.contentHashCode()
        return result
    }
}
open class TranslationUnit(): RealmObject(), Parcelable {
    companion object {
        @JvmField
        internal val CREATOR: Parcelable.Creator<TranslationUnit> = object: Parcelable.Creator<TranslationUnit> { //for Parcelable
            override fun createFromParcel(parcel: Parcel): TranslationUnit = TranslationUnit(parcel)
            override fun newArray(size: Int): Array<TranslationUnit?> = arrayOfNulls(size)
        }
    }
    //members
    var normalizedTarget: String? = null
        private set
    var displayTarget: String? = null
        private set
    var posTag: String? = null
        private set
    var confidence: Float = 0F
        private set
    var prefixWord: String? = null
        private set
    lateinit var backTranslations: RealmList<BackTranslation>
        private set
    override fun equals(other: Any?): Boolean { //generated code
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as TranslationUnit
        if (normalizedTarget != other.normalizedTarget) return false
        if (displayTarget != other.displayTarget) return false
        if (posTag != other.posTag) return false
        if (confidence != other.confidence) return false
        if (prefixWord != other.prefixWord) return false
        if (backTranslations != other.backTranslations) return false
        return true
    }
    override fun hashCode(): Int { //generated code
        var result = normalizedTarget.hashCode()
        result = 31 * result + displayTarget.hashCode()
        result = 31 * result + posTag.hashCode()
        result = 31 * result + confidence.hashCode()
        result = 31 * result + prefixWord.hashCode()
        result = 31 * result + backTranslations.hashCode()
        return result
    }
    //***Parcelable***//
    protected constructor(parcel: Parcel): this() {
        normalizedTarget = parcel.readString()
        displayTarget = parcel.readString()
        posTag = parcel.readString()
        confidence = parcel.readFloat()
        prefixWord = parcel.readString()
        parcel.readTypedList(backTranslations, BackTranslation.CREATOR)
    }
    override fun describeContents() = 0
    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(normalizedTarget)
        dest.writeString(displayTarget)
        dest.writeString(posTag)
        dest.writeFloat(confidence)
        dest.writeString(prefixWord)
        dest.writeTypedList(backTranslations)
    }
}
open class BackTranslation(): RealmObject(), Parcelable {
    companion object {
        @JvmField
        internal val CREATOR: Parcelable.Creator<BackTranslation> = object: Parcelable.Creator<BackTranslation> { //for Parcelable
            override fun createFromParcel(parcel: Parcel): BackTranslation = BackTranslation(parcel)
            override fun newArray(size: Int): Array<BackTranslation?> = arrayOfNulls(size)
        }
    }
    //members
    var normalizedText: String? = null
        private set
    var displayText: String? = null
        private set
    var numExamples: Int = 0
        private set
    var frequencyCount: Int = 0
        private set
    //***Parcelable***//
    protected constructor(parcel: Parcel): this() {
        normalizedText = parcel.readString()
        displayText = parcel.readString()
        numExamples = parcel.readInt()
        frequencyCount = parcel.readInt()
    }
    override fun describeContents() = 0
    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(normalizedText)
        dest.writeString(displayText)
        dest.writeInt(numExamples)
        dest.writeInt(frequencyCount)
    }
}