package ss.virovitica.honjaku.database

import io.realm.Realm
import ss.virovitica.honjaku.model.SentenceTranslation
import ss.virovitica.honjaku.model.WordTranslation

fun saveToRealm(sentenceTranslation: SentenceTranslation) {
    val realm = Realm.getDefaultInstance()
    realm.executeTransaction {
        if(sentenceTranslation.isCurrent) {
            val result1 = realm.where(SentenceTranslation::class.java).equalTo(SentenceTranslation.FIELD_IS_CURRENT, true).findAll()
                result1.deleteAllFromRealm()
            val result2 = realm.where(WordTranslation::class.java).equalTo(WordTranslation.FIELD_IS_CURRENT, true).findAll()
                result2.deleteAllFromRealm()
        }
        it.copyToRealm(sentenceTranslation)
    }
    realm.close()
}

fun saveToRealm(wordTranslation: WordTranslation) {
    val realm = Realm.getDefaultInstance()
    realm.executeTransaction {
        it.copyToRealm(wordTranslation)
    }
    realm.close()
}

fun getSentencesFromRealm(): List<SentenceTranslation> {
    val realm = Realm.getDefaultInstance()
    val result = realm.where(SentenceTranslation::class.java).equalTo(SentenceTranslation.FIELD_IS_CURRENT, false).findAll()
    val toReturn = realm.copyFromRealm(result)
    realm.close()
    return toReturn
}

fun getSentenceByIDFromRealm(id: String): SentenceTranslation? {
    val realm = Realm.getDefaultInstance()
    val result = realm.where(SentenceTranslation::class.java).equalTo(SentenceTranslation.FIELD_ID, id).findFirst()
    //result is null...?
    val toReturn = if(result != null) realm.copyFromRealm(result) else null
    realm.close()
    return toReturn
}

fun getWordsFromRealm(): List<WordTranslation> {
    val realm = Realm.getDefaultInstance()
    val result = realm.where(WordTranslation::class.java)
        .equalTo(WordTranslation.FIELD_IS_CURRENT, false).equalTo(WordTranslation.FIELD_IN_SENTENCE, false).findAll()
    val toReturn = realm.copyFromRealm(result)
    realm.close()
    return toReturn
}

fun getWordByIDFromRealm(id: String): WordTranslation? {
    val realm = Realm.getDefaultInstance()
    val result = realm.where(WordTranslation::class.java).equalTo(WordTranslation.FIELD_ID, id).findFirst()
    val toReturn = if(result != null) realm.copyFromRealm(result) else null
    realm.close()
    return toReturn
}

fun deleteSentenceFromRealm(id: String) {
    val realm = Realm.getDefaultInstance()
    realm.executeTransaction {
        it.where(SentenceTranslation::class.java).equalTo(SentenceTranslation.FIELD_ID, id)
            .equalTo(SentenceTranslation.FIELD_IS_CURRENT, false).findAll().deleteAllFromRealm()
    }
    realm.close()
}

fun deleteWordFromRealm(id: String) {
    val realm = Realm.getDefaultInstance()
    realm.executeTransaction {
        it.where(WordTranslation::class.java).equalTo(WordTranslation.FIELD_ID, id)
            .equalTo(WordTranslation.FIELD_IS_CURRENT, false).findAll().deleteAllFromRealm()
    }
    realm.close()
}