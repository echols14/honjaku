package ss.virovitica.honjaku.database

import ss.virovitica.honjaku.model.SentenceTranslation
import ss.virovitica.honjaku.model.WordTranslation

class Database {
    companion object {
        fun saveToDatabase(sentenceTranslation: SentenceTranslation) = saveToRealm(sentenceTranslation)
        fun saveToDatabase(wordTranslation: WordTranslation) = saveToRealm(wordTranslation)
        fun getSentenceTranslations() = getSentencesFromRealm()
        fun getSentenceByID(id: String) = getSentenceByIDFromRealm(id)
        fun getWordTranslations() = getWordsFromRealm()
        fun getWordByID(id: String) = getWordByIDFromRealm(id)
        fun deleteSentence(id: String) = deleteSentenceFromRealm(id)
        fun deleteWord(id: String) = deleteWordFromRealm(id)
    }
}