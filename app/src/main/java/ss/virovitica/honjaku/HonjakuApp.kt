package ss.virovitica.honjaku

import android.app.Activity
import android.app.Application
import android.support.v4.app.Fragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import io.realm.Realm
import ss.virovitica.honjaku.di.AppComponent
import ss.virovitica.honjaku.di.AppModule
import ss.virovitica.honjaku.di.DaggerAppComponent
import javax.inject.Inject

class HonjakuApp: Application(), HasActivityInjector, HasSupportFragmentInjector {
    companion object {
        lateinit var appComponent: AppComponent
            private set
    }

    @Inject lateinit var dispatchingAndroidActivityInjector: DispatchingAndroidInjector<Activity>
    @Inject lateinit var dispatchingAndroidFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidActivityInjector
    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidFragmentInjector

    override fun onCreate() {
        super.onCreate()
        appComponent = buildComponent()
        appComponent.inject(this)
        Realm.init(this)
    }

    private fun buildComponent(): AppComponent {
        return DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }
}