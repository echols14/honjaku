package ss.virovitica.honjaku.activity.save.di

import dagger.Module
import dagger.Provides
import ss.virovitica.honjaku.activity.save.presenter.ISavePresenter
import ss.virovitica.honjaku.activity.save.presenter.SavePresenter
import ss.virovitica.honjaku.activity.save.view.ISaveView
import ss.virovitica.honjaku.activity.save.view.SaveActivity
import ss.virovitica.honjaku.di.PerActivity

@Module
class SaveModule {
    @Provides @PerActivity
    fun provideView(activity: SaveActivity): ISaveView = activity
    @Provides @PerActivity
    fun providePresenter(presenter: SavePresenter): ISavePresenter = presenter
}