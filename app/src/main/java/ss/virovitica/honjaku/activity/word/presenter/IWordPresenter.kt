package ss.virovitica.honjaku.activity.word.presenter

interface IWordPresenter {
    var languageIndex: Int
    fun fetchWords(sentenceID: String)
}