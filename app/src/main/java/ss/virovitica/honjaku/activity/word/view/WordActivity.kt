package ss.virovitica.honjaku.activity.word.view

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_word.*
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import ss.virovitica.honjaku.R
import ss.virovitica.honjaku.activity.save.view.SaveActivity
import ss.virovitica.honjaku.activity.word.presenter.IWordPresenter
import ss.virovitica.honjaku.adapter.WordPagerAdapter
import ss.virovitica.honjaku.model.SentenceTranslation
import ss.virovitica.honjaku.utility.ToolbarManager
import javax.inject.Inject

class WordActivity: AppCompatActivity(), IWordView, ToolbarManager {
    companion object {
        const val WORD_INDEX_KEY = "ss.virovitica.honjaku.activity.word.view.WordActivity.WORD_INDEX_KEY"
        const val SENTENCE_ID_KEY = "ss.virovitica.honjaku.activity.word.view.WordActivity.SENTENCE_ID_KEY"
        const val FROM_SAVED_SENTENCE_KEY = "ss.virovitica.honjaku.activity.word.view.WordActivity.FROM_SAVED_SENTENCE_KEY"
    }

    //members
    override val ctx: Context
        get() = this
    override val toolbar: Toolbar by lazy { find<Toolbar>(R.id.toolbar) }
    private lateinit var presenter: IWordPresenter
    private lateinit var adapter: WordPagerAdapter
    private var startPageIndex = -1
    private var isNew: Boolean = false
    private lateinit var sentenceID: String

    //functions

    @Inject
    fun inject(presenter: IWordPresenter) {
        this.presenter = presenter
    }

    //***AppCompatActivity***//

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_word)
        val fromSavedSentence = intent.getBooleanExtra(FROM_SAVED_SENTENCE_KEY, false)
        //set up the toolbar
        if(!fromSavedSentence) initSaveToolbar()
        enableHomeAsUp { onBackPressed() }
        if(fromSavedSentence) toolbarTitle = getString(R.string.saved_sentence)
        //set up the pager adapter
        adapter = WordPagerAdapter(supportFragmentManager)
        wordPager.adapter = adapter
        //read the info from the intent of what sentence and what word we want to show first
        val sentenceID = intent.getStringExtra(SENTENCE_ID_KEY)
        startPageIndex = intent.getIntExtra(WORD_INDEX_KEY, -1)
        isNew = true
        if(sentenceID != null) {
            presenter.fetchWords(sentenceID)
            this.sentenceID = sentenceID
        }
    }

    //***IWordView***//

    override fun setWords(sentence: SentenceTranslation) {
        adapter.words = sentence.toWordArray()
        toolbarTitle = sentence.sourceLanguage
        if (isNew) { //if the article was newly created from a click in the MainActivity, select the article clicked
            wordPager.currentItem = startPageIndex
            isNew = false
        }
    }

    //***ToolbarManager***//

    override fun saveButton(savable: Boolean) {
        startActivity<SaveActivity>(SaveActivity.KEY_SAVABLE to savable,
            SaveActivity.KEY_SENTENCE_ID to sentenceID, SaveActivity.KEY_WORD_INDEX to wordPager.currentItem)
    }
}