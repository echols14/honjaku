package ss.virovitica.honjaku.activity.singleWord.presenter

interface ISingleWordPresenter {
    fun getWord(wordID: String)
}