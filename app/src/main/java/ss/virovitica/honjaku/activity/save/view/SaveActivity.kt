package ss.virovitica.honjaku.activity.save.view

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_save.*
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import ss.virovitica.honjaku.R
import ss.virovitica.honjaku.activity.save.presenter.ISavePresenter
import ss.virovitica.honjaku.adapter.SaveRecyclerAdapter
import ss.virovitica.honjaku.utility.ToolbarManager
import javax.inject.Inject

class SaveActivity: AppCompatActivity(), ISaveView, ToolbarManager {
    companion object {
        const val KEY_SAVABLE = "ss.virovitica.honjaku.activity.save.view.SaveActivity.KEY_SAVABLE"
        const val KEY_SENTENCE_ID = "ss.virovitica.honjaku.activity.save.view.SaveActivity.KEY_SENTENCE_ID"
        const val KEY_WORD_INDEX = "ss.virovitica.honjaku.activity.save.view.SaveActivity.KEY_WORD_INDEX"
    }

    //members
    override val ctx: Context
        get() = this
    override val toolbar: Toolbar by lazy { find<Toolbar>(R.id.toolbar) }
    private lateinit var adapter: SaveRecyclerAdapter
    private lateinit var presenter: ISavePresenter

    //functions

    @Inject
    internal fun inject(presenterIn: ISavePresenter) {
        presenter = presenterIn
        adapter = SaveRecyclerAdapter(this, presenter, layoutInflater)
    }

    //***AppCompatActivity***//

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_save)
        //set up the toolbar
        val savable = intent.getBooleanExtra(KEY_SAVABLE, false)
        if(savable) initAddSavedToolbar()
        enableHomeAsUp { onBackPressed() }
        toolbarTitle = getString(R.string.saved_title)
        //set up the RecyclerView
        saveRecycler.layoutManager = LinearLayoutManager(this)
        saveRecycler.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        saveRecycler.adapter = adapter
        adapter.loadData(presenter.getAdapterWrappers())
    }

    //***ISaveView***//

    override fun deleteSuccessful() {
        adapter.loadData(presenter.getAdapterWrappers())
        toast(getString(R.string.translation_deleted))
    }

    //***ToolbarManager***//

    override fun addButton() {
        val sentenceID: String? = intent.getStringExtra(KEY_SENTENCE_ID)
        val wordIndex = intent.getIntExtra(KEY_WORD_INDEX, -1)
        try {
            if (sentenceID == null) throw SentenceException("sentence ID not sent")
            else {
                val sentenceToSave = presenter.getSentence(sentenceID)
                    ?: throw SentenceException("sentence not retrieved through presenter")
                if (wordIndex == -1) { //trying to save a sentence
                    sentenceToSave.isCurrent = false
                    for(word in sentenceToSave.words) {
                        word.isCurrent = false
                        word.inSentence = true
                    }
                    presenter.save(sentenceToSave)
                }
                else { //trying to save a word
                    val wordToSave = sentenceToSave.words[wordIndex]
                        ?: throw SentenceException("word not found in sentence")
                    wordToSave.isCurrent = false
                    wordToSave.inSentence = false
                    presenter.save(wordToSave)
                }
                adapter.loadData(presenter.getAdapterWrappers())
                toast(getString(R.string.translation_saved))
            }
        }
        catch(e: SentenceException) {
            println(e.toString())
            super.addButton()
        }
    }

    private inner class SentenceException(message: String): Exception(message)
}