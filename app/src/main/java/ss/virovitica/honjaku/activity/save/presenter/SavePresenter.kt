package ss.virovitica.honjaku.activity.save.presenter

import ss.virovitica.honjaku.R
import ss.virovitica.honjaku.activity.save.view.ISaveView
import ss.virovitica.honjaku.adapter.SaveRecyclerAdapter.AdapterWrapper
import ss.virovitica.honjaku.database.Database
import ss.virovitica.honjaku.model.SentenceTranslation
import ss.virovitica.honjaku.model.WordTranslation
import javax.inject.Inject

class SavePresenter @Inject constructor(private val view: ISaveView): ISavePresenter {
    override fun getSentence(sentenceID: String): SentenceTranslation? {
        return Database.getSentenceByID(sentenceID)
    }

    override fun save(sentenceToSave: SentenceTranslation) {
        Database.saveToDatabase(sentenceToSave)
    }

    override fun save(wordTranslation: WordTranslation) {
        Database.saveToDatabase(wordTranslation)
    }

    override fun deleteSentence(sentenceID: String) {
        Database.deleteSentence(sentenceID)
        view.deleteSuccessful()
    }

    override fun deleteWord(wordID: String) {
        Database.deleteWord(wordID)
        view.deleteSuccessful()
    }

    override fun getAdapterWrappers(): List<AdapterWrapper> {
        val savedSentences = Database.getSentenceTranslations()
        val savedWords = Database.getWordTranslations()
        val wrapperList = ArrayList<AdapterWrapper>(2 + savedSentences.size + savedWords.size)
        //sentences header
        wrapperList.add(AdapterWrapper(R.layout.view_section_header, view.ctx.getString(R.string.saved_sentences)))
        //sentences
        for(sentence in savedSentences) {
            wrapperList.add(AdapterWrapper(R.layout.view_sentence_summary, sentence))
        }
        //words header
        wrapperList.add(AdapterWrapper(R.layout.view_section_header, view.ctx.getString(R.string.saved_words)))
        //words
        for(word in savedWords) {
            wrapperList.add(AdapterWrapper(R.layout.view_saved_word_summary, word))
        }
        return wrapperList
    }
}