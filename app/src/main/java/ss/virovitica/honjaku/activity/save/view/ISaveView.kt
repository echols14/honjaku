package ss.virovitica.honjaku.activity.save.view

import ss.virovitica.honjaku.utility.IView

interface ISaveView: IView {
    fun deleteSuccessful()
}