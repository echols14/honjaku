package ss.virovitica.honjaku.activity.main.view

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast
import ss.virovitica.honjaku.R
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.view.KeyEvent
import dagger.android.AndroidInjection
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import ss.virovitica.honjaku.activity.main.presenter.IMainPresenter
import ss.virovitica.honjaku.activity.save.view.SaveActivity
import ss.virovitica.honjaku.activity.save.view.SaveActivity.Companion.KEY_SAVABLE
import ss.virovitica.honjaku.utility.ToolbarManager
import javax.inject.Inject

class MainActivity : AppCompatActivity(), IMainView, ToolbarManager {
    //members
    override val ctx: Context
        get() = this
    override val toolbar: Toolbar by lazy { find<Toolbar>(R.id.toolbar) }
    private lateinit var presenter: IMainPresenter
    private var inputTextString = ""
    private var loading = false

    //functions

    @Inject fun inject(presenter: IMainPresenter) {
        this.presenter = presenter
    }

    //***AppCompatActivity***//

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //set up the toolbar
        initViewSavedToolbar()
        toolbarTitle = getString(R.string.app_name)
        //set up both spinners
        val inputSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.languagesArray, android.R.layout.simple_spinner_item)
        inputSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        inputLanguageSpinner.adapter = inputSpinnerAdapter
        inputLanguageSpinner.setSelection(0)
        inputLanguageSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Log.i("inputLanguageSpinner", "Nothing selected")
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                translateButton.isEnabled = verifyInput()
                updateLangPreference()
            }
        }
        inputLanguageSpinner.isEnabled = false
        val outputSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.languagesArray, android.R.layout.simple_spinner_item)
        outputSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        outputLanguageSpinner.adapter = outputSpinnerAdapter
        outputLanguageSpinner.setSelection(presenter.languageIndex)
        outputLanguageSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Log.i("outputLanguageSpinner", "Nothing selected")
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                translateButton.isEnabled = verifyInput()
                updateLangPreference()
            }
        }
        if(presenter.toEnglish == 1) swapLanguages()
        //set up the button for swap languages
        swapLanguagesButton.setOnClickListener { swapLanguages() }
        //set up listener for input EditText
        inputText.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                inputTextString = s.toString()
                translateButton.isEnabled = verifyInput()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        //set up listener for the imeOptions thing on the input EditText
        inputText.setOnEditorActionListener(object: TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    attemptSend()
                    return true //event handled
                }
                return false //event not handled here
            }
        })
        //set up the clear button
        clearButton.setOnClickListener { inputText.setText("") }
        //set up the button for translate
        translateButton.setOnClickListener { attemptSend() }
    }

    override fun onDestroy() {
        presenter.finish()
        super.onDestroy()
    }

    //***IMainView***//

    override fun swapLanguages() {
        inputLanguageSpinner.isEnabled = false
        outputLanguageSpinner.isEnabled = false
        val inputNotEnglish = (inputLanguageSpinner.selectedItemPosition != 0)
        if(inputNotEnglish){
            presenter.toEnglish = 0
            outputLanguageSpinner.setSelection(inputLanguageSpinner.selectedItemPosition)
            inputLanguageSpinner.setSelection(0)
            outputLanguageSpinner.isEnabled = true
        }
        else {
            presenter.toEnglish = 1
            inputLanguageSpinner.setSelection(outputLanguageSpinner.selectedItemPosition)
            outputLanguageSpinner.setSelection(0)
            inputLanguageSpinner.isEnabled = true
        }
    }

    override fun attemptSend() {
        if (inputLanguageSpinner.selectedItemPosition == outputLanguageSpinner.selectedItemPosition) toast(getString(R.string.select_differing_languages))
        else if (!verifyInput()) toast(getString(R.string.enter_text_to_translate))
        else if (loading) toast(getString(R.string.please_wait))
        else sendInput()
    }

    override fun onFinishedSend() {
        loading = false
        translateButton.isEnabled = true
    }

    //***ToolbarManager***//

    override fun saveButton(savable: Boolean) {
        startActivity<SaveActivity>(KEY_SAVABLE to savable)
    }

    //***private***//

    private fun verifyInput(): Boolean {
        return (inputTextString != "")
    }

    private fun sendInput() {
        translateButton.isEnabled = false
        loading = true
        presenter.fetchTranslation(inputTextString, inputLanguageSpinner.selectedItemPosition, outputLanguageSpinner.selectedItemPosition)
    }

    private fun updateLangPreference() {
        if(inputLanguageSpinner.selectedItemPosition != 0) presenter.languageIndex = inputLanguageSpinner.selectedItemPosition
        else presenter.languageIndex = outputLanguageSpinner.selectedItemPosition
    }
}
