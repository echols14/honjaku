package ss.virovitica.honjaku.activity.main.di

import dagger.Module
import dagger.Provides
import ss.virovitica.honjaku.activity.main.presenter.IMainPresenter
import ss.virovitica.honjaku.activity.main.presenter.MainPresenter
import ss.virovitica.honjaku.activity.main.view.IMainView
import ss.virovitica.honjaku.activity.main.view.MainActivity
import ss.virovitica.honjaku.di.PerActivity

@Module
class MainModule {
    @Provides @PerActivity
    fun provideView(activity: MainActivity): IMainView = activity
    @Provides @PerActivity
    fun providePresenter(presenter: MainPresenter): IMainPresenter = presenter
}