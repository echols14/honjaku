package ss.virovitica.honjaku.activity.singleWord.presenter

import ss.virovitica.honjaku.activity.singleWord.view.ISingleWordView
import ss.virovitica.honjaku.database.Database
import javax.inject.Inject

class SingleWordPresenter @Inject constructor(private val view: ISingleWordView): ISingleWordPresenter {
    override fun getWord(wordID: String) {
        val word = Database.getWordByID(wordID)
        if(word != null) view.setWord(word)
    }
}