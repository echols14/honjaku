package ss.virovitica.honjaku.activity.save.presenter

import ss.virovitica.honjaku.adapter.SaveRecyclerAdapter
import ss.virovitica.honjaku.model.SentenceTranslation
import ss.virovitica.honjaku.model.WordTranslation

interface ISavePresenter {
    fun getSentence(sentenceID: String): SentenceTranslation?
    fun save(sentenceToSave: SentenceTranslation)
    fun save(wordTranslation: WordTranslation)
    fun deleteSentence(sentenceID: String)
    fun deleteWord(wordID: String)
    fun getAdapterWrappers(): List<SaveRecyclerAdapter.AdapterWrapper>
}