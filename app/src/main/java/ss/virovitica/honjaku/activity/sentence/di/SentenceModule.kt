package ss.virovitica.honjaku.activity.sentence.di

import dagger.Module
import dagger.Provides
import ss.virovitica.honjaku.activity.sentence.presenter.ISentencePresenter
import ss.virovitica.honjaku.activity.sentence.presenter.SentencePresenter
import ss.virovitica.honjaku.activity.sentence.view.ISentenceView
import ss.virovitica.honjaku.activity.sentence.view.SentenceActivity
import ss.virovitica.honjaku.di.PerActivity

@Module
class SentenceModule {
    @Provides @PerActivity
    fun provideView(activity: SentenceActivity): ISentenceView = activity
    @Provides @PerActivity
    fun providePresenter(presenter: SentencePresenter): ISentencePresenter = presenter
}