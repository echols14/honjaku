package ss.virovitica.honjaku.activity.word.di

import dagger.Module
import dagger.Provides
import ss.virovitica.honjaku.activity.word.presenter.IWordPresenter
import ss.virovitica.honjaku.activity.word.presenter.WordPresenter
import ss.virovitica.honjaku.activity.word.view.IWordView
import ss.virovitica.honjaku.activity.word.view.WordActivity
import ss.virovitica.honjaku.di.PerActivity

@Module
class WordModule {
    @Provides @PerActivity
    fun provideView(activity: WordActivity): IWordView = activity
    @Provides @PerActivity
    fun providePresenter(presenter: WordPresenter): IWordPresenter = presenter
}