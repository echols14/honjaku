package ss.virovitica.honjaku.activity.word.presenter

import ss.virovitica.honjaku.activity.main.presenter.MainPresenter
import ss.virovitica.honjaku.activity.word.view.IWordView
import ss.virovitica.honjaku.database.Database.Companion.getSentenceByID
import ss.virovitica.honjaku.utility.DelegatesExt
import javax.inject.Inject

class WordPresenter @Inject constructor(private val view: IWordView): IWordPresenter {
    //members
    override var languageIndex: Int by DelegatesExt.intPreference(view.ctx, MainPresenter.LANGUAGE_INDEX_PREFERENCE, 1)

    //functions

    //***IWordPresenter***//

    override fun fetchWords(sentenceID: String) {
        val sentence = getSentenceByID(sentenceID)
        if(sentence != null) view.setWords(sentence)
    }
}