package ss.virovitica.honjaku.activity.main.presenter

import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import ss.virovitica.honjaku.R
import ss.virovitica.honjaku.activity.main.view.IMainView
import ss.virovitica.honjaku.activity.sentence.view.SentenceActivity
import ss.virovitica.honjaku.activity.sentence.view.SentenceActivity.Companion.SENTENCE_ID_KEY
import ss.virovitica.honjaku.interactor.ITranslationInteractor
import ss.virovitica.honjaku.model.SentenceTranslation
import ss.virovitica.honjaku.database.Database.Companion.saveToDatabase
import ss.virovitica.honjaku.utility.DelegatesExt
import ss.virovitica.honjaku.utility.Listener
import javax.inject.Inject

class MainPresenter @Inject constructor(private val view: IMainView, private val translationInteractor: ITranslationInteractor): IMainPresenter, Listener<SentenceTranslation> {
    companion object {
        internal const val LANGUAGE_INDEX_PREFERENCE = "ss.virovitica.honjaku.activity.main.LANGUAGE_INDEX_PREFERENCE"
        internal const val TO_ENGLISH_BOOL_PREFERENCE = "ss.virovitica.honjaku.activity.main.TO_ENGLISH_BOOL_PREFERENCE"
    }

    //members
    override var languageIndex: Int by DelegatesExt.intPreference(view.ctx, LANGUAGE_INDEX_PREFERENCE, 1)
    override var toEnglish: Int by DelegatesExt.intPreference(view.ctx, TO_ENGLISH_BOOL_PREFERENCE, 0)
    private var tokenizedID: String = ""

    //functions

    //***IMainPresenter***//

    override fun fetchTranslation(toTranslate: String, fromLangIndex: Int, destLangIndex: Int) {
        translationInteractor.fetchTokenTranslation(toTranslate, fromLangIndex, destLangIndex, this)
    }

    override fun finish() {
        translationInteractor.unsubscribe()
    }

    //***Listener***//

    override fun onSuccess(result: SentenceTranslation) {
        view.onFinishedSend()
        if(toEnglish == 0) { //from English
            result.sourceLanguage = view.ctx.resources.getStringArray(R.array.languagesArray)[0]
            result.targetLanguage = view.ctx.resources.getStringArray(R.array.languagesArray)[languageIndex]
        }
        else { //to English
            result.sourceLanguage = view.ctx.resources.getStringArray(R.array.languagesArray)[languageIndex]
            result.targetLanguage = view.ctx.resources.getStringArray(R.array.languagesArray)[0]
        }
        result.isCurrent = true
        for(word in result.words) {
            word.isCurrent = true
        }
        saveToDatabase(result)
        tokenizedID = result.id
        if(tokenizedID != ""){ //we did successfully get a sentence
            view.ctx.startActivity<SentenceActivity>(SENTENCE_ID_KEY to tokenizedID)
            tokenizedID = ""
        }
    }

    override fun onError() {
        view.onFinishedSend()
        view.ctx.toast(view.ctx.getString(R.string.translation_failed))
    }
}