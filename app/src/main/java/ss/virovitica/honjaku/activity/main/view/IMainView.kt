package ss.virovitica.honjaku.activity.main.view

import ss.virovitica.honjaku.utility.IView

interface IMainView: IView {
    fun swapLanguages()
    fun attemptSend()
    fun onFinishedSend()
}