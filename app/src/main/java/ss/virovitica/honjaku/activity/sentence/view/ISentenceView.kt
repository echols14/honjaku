package ss.virovitica.honjaku.activity.sentence.view

import ss.virovitica.honjaku.model.SentenceTranslation
import ss.virovitica.honjaku.utility.IView

interface ISentenceView: IView {
    var currentSentenceID: String
    fun setSentenceList(sentence: SentenceTranslation)
}