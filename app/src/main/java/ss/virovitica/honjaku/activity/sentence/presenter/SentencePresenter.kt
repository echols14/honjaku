package ss.virovitica.honjaku.activity.sentence.presenter

import ss.virovitica.honjaku.activity.main.presenter.MainPresenter
import ss.virovitica.honjaku.activity.sentence.view.ISentenceView
import ss.virovitica.honjaku.database.Database.Companion.getSentenceByID
import ss.virovitica.honjaku.utility.DelegatesExt
import javax.inject.Inject

class SentencePresenter @Inject constructor(private val view: ISentenceView): ISentencePresenter {
    //members
    override var languageIndex: Int by DelegatesExt.intPreference(view.ctx, MainPresenter.LANGUAGE_INDEX_PREFERENCE, 1)

    //functions

    //***ISentencePresenter***//

    override fun fetchSentence(id: String, tokenized: Boolean) {
        val sentence = getSentenceByID(id)
        if(sentence != null) view.setSentenceList(sentence)
    }
}