package ss.virovitica.honjaku.activity.singleWord.di

import dagger.Module
import dagger.Provides
import ss.virovitica.honjaku.activity.singleWord.presenter.ISingleWordPresenter
import ss.virovitica.honjaku.activity.singleWord.presenter.SingleWordPresenter
import ss.virovitica.honjaku.activity.singleWord.view.ISingleWordView
import ss.virovitica.honjaku.activity.singleWord.view.SingleWordActivity
import ss.virovitica.honjaku.di.PerActivity

@Module
class SingleWordModule {
    @Provides @PerActivity
    fun provideView(activity: SingleWordActivity): ISingleWordView = activity
    @Provides @PerActivity
    fun providePresenter(presenter: SingleWordPresenter): ISingleWordPresenter = presenter
}