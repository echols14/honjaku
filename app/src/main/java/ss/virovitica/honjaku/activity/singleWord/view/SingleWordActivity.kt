package ss.virovitica.honjaku.activity.singleWord.view

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import dagger.android.AndroidInjection
import org.jetbrains.anko.find
import ss.virovitica.honjaku.R
import ss.virovitica.honjaku.activity.singleWord.presenter.ISingleWordPresenter
import ss.virovitica.honjaku.fragment.WordFragment
import ss.virovitica.honjaku.model.WordTranslation
import ss.virovitica.honjaku.utility.ToolbarManager
import javax.inject.Inject

class SingleWordActivity: AppCompatActivity(), ISingleWordView, ToolbarManager {
    companion object {
        const val KEY_WORD_ID = "ss.virovitica.honjaku.activity.singleWord.view.SingleWordActivity.KEY_WORD_ID"
    }

    //members
    override val ctx: Context
        get() = this
    override val toolbar: Toolbar by lazy { find<Toolbar>(R.id.toolbar) }
    private lateinit var presenter: ISingleWordPresenter
    private lateinit var word: WordTranslation

    //functions

    @Inject
    fun inject(presenter: ISingleWordPresenter) {
        this.presenter = presenter
    }

    //***AppCompatActivity***//

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_word)
        //set up the toolbar
        enableHomeAsUp { onBackPressed() }
        //find the word we need
        val wordID = intent.getStringExtra(KEY_WORD_ID)
        if(wordID != null) {
            presenter.getWord(wordID)
            //add the word fragment
            val fragment = supportFragmentManager.findFragmentById(R.id.frameLayout)
            if (fragment == null) {
                val newFragment = WordFragment.newInstance(word)
                supportFragmentManager.beginTransaction().add(R.id.frameLayout, newFragment).commit()
            }
        }
    }

    //***ISingleWordView***///

    override fun setWord(word: WordTranslation) {
        this.word = word
        toolbarTitle = word.sourceLanguage
    }
}