package ss.virovitica.honjaku.activity.main.presenter

interface IMainPresenter {
    var languageIndex: Int
    var toEnglish: Int
    fun fetchTranslation(toTranslate: String, fromLangIndex: Int, destLangIndex: Int)
    fun finish()
}