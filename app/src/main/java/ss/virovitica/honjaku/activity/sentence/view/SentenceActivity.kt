package ss.virovitica.honjaku.activity.sentence.view

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_sentence.*
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import ss.virovitica.honjaku.R
import ss.virovitica.honjaku.activity.save.view.SaveActivity
import ss.virovitica.honjaku.activity.save.view.SaveActivity.Companion.KEY_SAVABLE
import ss.virovitica.honjaku.activity.save.view.SaveActivity.Companion.KEY_SENTENCE_ID
import ss.virovitica.honjaku.activity.sentence.presenter.ISentencePresenter
import ss.virovitica.honjaku.adapter.SentenceRecyclerAdapter
import ss.virovitica.honjaku.model.SentenceTranslation
import ss.virovitica.honjaku.utility.ToolbarManager
import javax.inject.Inject

class SentenceActivity: AppCompatActivity(), ISentenceView, ToolbarManager {
    companion object {
        const val SENTENCE_ID_KEY = "ss.virovitica.honjaku.activity.sentence.view.SentenceActivity.SENTENCE_ID_KEY"
        const val SENTENCE_SAVED_BOOL_KEY = "ss.virovitica.honjaku.activity.sentence.view.SentenceActivity.SENTENCE_SAVED_BOOL_KEY"
    }
    //members
    override val ctx: Context
        get() = this
    override val toolbar: Toolbar by lazy { find<Toolbar>(R.id.toolbar) }
    override lateinit var currentSentenceID: String
    private lateinit var presenter: ISentencePresenter
    private lateinit var adapter: SentenceRecyclerAdapter
    private var fromSaved = false

    //functions

    @Inject
    fun inject(presenter: ISentencePresenter, adapter: SentenceRecyclerAdapter) {
        this.presenter = presenter
        this.adapter = adapter
    }

    //***AppCompatActivity***//

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sentence)
        //set up the toolbar
        fromSaved = intent.getBooleanExtra(SENTENCE_SAVED_BOOL_KEY, false)
        enableHomeAsUp { onBackPressed() }
        //set up the RecyclerView
        sentenceRecycler.layoutManager = LinearLayoutManager(this)
        sentenceRecycler.addItemDecoration(DividerItemDecoration(sentenceRecycler.context, DividerItemDecoration.VERTICAL))
        sentenceRecycler.adapter = adapter
        //pull the SentenceTranslation IDs from the Intent
        val tokenizedSentenceID = intent.getStringExtra(SENTENCE_ID_KEY)
        //send a request to database to pull the appropriate sentences
        if(tokenizedSentenceID != null) presenter.fetchSentence(tokenizedSentenceID, true)
    }

    //***ISentenceView***//

    override fun setSentenceList(sentence: SentenceTranslation) {
        currentSentenceID = sentence.id
        adapter.sentence = sentence
        toolbarTitle = getString(R.string.a_to_b, sentence.sourceLanguage, sentence.targetLanguage)
        if(fromSaved) { //from saved
            adapter.fromSaved = true
            getString(R.string.saved_sentence)
        }
        else initSaveToolbar() //from translation
        sentenceFullTranslationText.text = sentence.printSimple()
    }

    //***ToolbarManager***//

    override fun saveButton(savable: Boolean) {
        startActivity<SaveActivity>(KEY_SAVABLE to savable, KEY_SENTENCE_ID to currentSentenceID)
    }
}