package ss.virovitica.honjaku.activity.sentence.presenter

interface ISentencePresenter {
    var languageIndex: Int
    fun fetchSentence(id: String, tokenized: Boolean)
}