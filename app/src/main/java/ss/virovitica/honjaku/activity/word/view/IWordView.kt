package ss.virovitica.honjaku.activity.word.view

import ss.virovitica.honjaku.model.SentenceTranslation
import ss.virovitica.honjaku.utility.IView

interface IWordView: IView {
    fun setWords(sentence: SentenceTranslation)
}