package ss.virovitica.honjaku.activity.singleWord.view

import ss.virovitica.honjaku.model.WordTranslation
import ss.virovitica.honjaku.utility.IView

interface ISingleWordView: IView {
    fun setWord(word: WordTranslation)
}