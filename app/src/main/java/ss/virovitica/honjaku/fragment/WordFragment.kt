package ss.virovitica.honjaku.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import dagger.android.support.AndroidSupportInjection
import org.jetbrains.anko.find
import ss.virovitica.honjaku.R
import ss.virovitica.honjaku.model.WordTranslation

class WordFragment: Fragment() {
    companion object {
        private const val KEY_WORD = "ss.virovitica.honjaku.fragment.WordFragment.KEY_WORD"
        fun newInstance(word: WordTranslation): WordFragment {
            val newFragment = WordFragment()
            val bundle = Bundle()
            bundle.putParcelable(KEY_WORD, word)
            newFragment.arguments = bundle
            return newFragment
        }
    }

    //members
    private lateinit var word: WordTranslation

    //functions

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        val args = arguments
        if (args != null) {
            word = args.getParcelable(KEY_WORD) ?: WordTranslation()
        }
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_word, container, false)
        //grab the views
        //load the word  text
        (v.find(R.id.headerText) as TextView).text = word.displaySource
        (v.find(R.id.detailText) as TextView).text = word.prettyTranslationDetail()
        return v
    }
}