package ss.virovitica.honjaku.fragment

import dagger.Module

@Module
class WordFragmentModule {
    //no view, presenter, or interactor
    //all functions are contained in the WordFragment class since it's just a one-time display
}