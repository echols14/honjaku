package ss.virovitica.honjaku.interactor

import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import ss.virovitica.honjaku.model.Languages
import ss.virovitica.honjaku.model.SentenceTranslation
import ss.virovitica.honjaku.model.response.microsoft.MicrosoftResponse
import ss.virovitica.honjaku.model.response.microsoft.MicrosoftResponseMapper
import ss.virovitica.honjaku.net.MicrosoftClient
import ss.virovitica.honjaku.utility.Listener
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MicrosoftInteractor @Inject constructor(private val client: MicrosoftClient): ITranslationInteractor {
    //members
    private var tokenDisposable: CompositeDisposable? = null
    private var fullDisposable: CompositeDisposable? = null

    //functions

    //***ITranslationInteractor***//

    override fun fetchTokenTranslation(toTranslate: String, fromLangIndex: Int, destLangIndex: Int, listener: Listener<SentenceTranslation>) {
        tokenDisposable = CompositeDisposable()
        tokenDisposable?.add(
            client.getSentenceTranslation(Languages.spinnerIndexToLangCodeMicrosoft(fromLangIndex),
                Languages.spinnerIndexToLangCodeMicrosoft(destLangIndex), toTranslate, true)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(object : DisposableObserver<MicrosoftResponse>() {
                    override fun onNext(response: MicrosoftResponse) {
                        Log.i("MicrosoftInteractor", "token translation successful")
                        listener.onSuccess(MicrosoftResponseMapper.responseToModel(response))
                    }
                    override fun onError(e: Throwable) {
                        Log.e("MicrosoftInteractor", e.toString())
                        listener.onError()
                    }
                    override fun onComplete() {}
                })
        )
    }

    override fun unsubscribe() {
        if(tokenDisposable?.isDisposed == false) tokenDisposable?.dispose()
        tokenDisposable = null
        if(fullDisposable?.isDisposed == false) fullDisposable?.dispose()
        fullDisposable = null
    }
}