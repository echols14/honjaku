package ss.virovitica.honjaku.interactor

import ss.virovitica.honjaku.model.SentenceTranslation
import ss.virovitica.honjaku.utility.Listener

interface ITranslationInteractor {
    fun fetchTokenTranslation(toTranslate: String, fromLangIndex: Int, destLangIndex: Int, listener: Listener<SentenceTranslation>)
    fun unsubscribe()
}