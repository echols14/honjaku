package ss.virovitica.honjaku.net

import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query
import ss.virovitica.honjaku.di.NetModule.Companion.MICROSOFT_RETROFIT
import ss.virovitica.honjaku.model.response.microsoft.MicrosoftResponse
import ss.virovitica.honjaku.utility.writeMicrosoftRequestJson
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class MicrosoftClient @Inject constructor(@Named(MICROSOFT_RETROFIT) private val retrofit: Retrofit): ApiClient<MicrosoftResponse> {
    companion object {
        const val TIMEOUT_SECONDS: Long = 10
        const val BASE_URL = "https://api.cognitive.microsofttranslator.com/"
        private const val PATH = "dictionary/lookup"
        private const val AUTH = "Ocp-Apim-Subscription-Key"
        private const val API_VERSION = "api-version"
        private const val FROM = "from"
        private const val TO = "to"

        private const val API_KEY = "1d22a1a1f41741af8011aab11a1ce9b6"
        private const val API_VERSION_VAL = "3.0"
    }

    override fun getSentenceTranslation(fromLangCode: String, destLangCode: String, phrase: String, tokenize: Boolean): Observable<MicrosoftResponse> {
        //build a client using retrofit, then get an Observable object from it
        return retrofit.create(RetrofitClient::class.java).getSentenceObservable(API_KEY, API_VERSION_VAL, fromLangCode, destLangCode,
            RequestBody.create(MediaType.parse("application/json"), writeMicrosoftRequestJson(phrase, tokenize)))
    }

    private interface RetrofitClient {
        @POST(PATH)
        fun getSentenceObservable(@Header(AUTH) apiKey: String, @Query(API_VERSION) apiVersion: String,
                                  @Query(FROM) fromLangCode: String, @Query(TO) toLangCode: String,
                                  @Body jsonRequestBody: RequestBody): Observable<MicrosoftResponse>
    }
}