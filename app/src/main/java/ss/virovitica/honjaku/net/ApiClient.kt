package ss.virovitica.honjaku.net

import io.reactivex.Observable

interface ApiClient<T> {
    fun getSentenceTranslation(fromLangCode: String, destLangCode: String, phrase: String, tokenize: Boolean): Observable<T>
}