package ss.virovitica.honjaku.utility

import android.support.v7.graphics.drawable.DrawerArrowDrawable
import android.support.v7.widget.Toolbar
import org.jetbrains.anko.toast
import ss.virovitica.honjaku.R

interface ToolbarManager {
    //members
    val toolbar: Toolbar
    var toolbarTitle: String
        get() = toolbar.title.toString()
        set(value){
            toolbar.title = value
        }

    //functions

    fun initSaveToolbar(){
        toolbar.inflateMenu(R.menu.menu_save)
        toolbar.setOnMenuItemClickListener{
            when(it.itemId){
                R.id.action_save -> saveButton(true)
                else -> toolbar.ctx.toast("Unknown option")
            }
            true
        }
    }

    fun initViewSavedToolbar(){
        toolbar.inflateMenu(R.menu.menu_view_saved)
        toolbar.setOnMenuItemClickListener{
            when(it.itemId){
                R.id.action_view_saved -> saveButton(false)
                else -> toolbar.ctx.toast("Unknown option")
            }
            true
        }
    }

    fun initAddSavedToolbar() {
        toolbar.inflateMenu(R.menu.menu_add_saved)
        toolbar.setOnMenuItemClickListener{
            when(it.itemId){
                R.id.action_add -> addButton()
                else -> toolbar.ctx.toast("Unknown option")
            }
            true
        }
    }

    fun enableHomeAsUp(up: () -> Unit) {
        toolbar.navigationIcon = createUpDrawable()
        toolbar.setNavigationOnClickListener { up() }
    }

    private fun createUpDrawable() = DrawerArrowDrawable(toolbar.ctx).apply { progress = 1f }

    fun saveButton(savable: Boolean){
        toolbar.ctx.toast("Unknown option")
    }

    fun addButton() {
        toolbar.ctx.toast(toolbar.ctx.getString(R.string.failed_save))
    }
}