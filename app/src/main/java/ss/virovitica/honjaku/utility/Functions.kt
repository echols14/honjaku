package ss.virovitica.honjaku.utility

import java.lang.StringBuilder
import java.util.*

fun writeMicrosoftRequestJson(phrase: String, tokenize: Boolean): String {
    //start writing the body
    val out = StringBuilder()
    out.append("[\n")
    if(tokenize) {
        //write the phrases to be translated
        val scanner = Scanner(phrase)
        while (scanner.hasNext()) {
            out.append("{\"Text\": \"")
            out.append(scanner.next())
            out.append("\"}")
            if (scanner.hasNext()) out.append(",")
            out.append("\n")
        }
    }
    else {
        out.append("{\"Text\": \"")
        out.append(phrase)
        out.append("\"}")
    }
    //close the body
    out.append("]")
    return out.toString()
}

