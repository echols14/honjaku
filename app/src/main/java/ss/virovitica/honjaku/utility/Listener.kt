package ss.virovitica.honjaku.utility

interface Listener<T> {
    fun onSuccess(result: T)
    fun onError()
}