package ss.virovitica.honjaku.utility

import android.content.Context
import android.view.View
import kotlin.reflect.KProperty

val View.ctx: Context
    get() = context

object DelegatesExt {
    fun intPreference(context: Context, name: String, default: Int) = IntPreference(context, name, default)
}

class IntPreference(val context: Context, val name: String, private val default: Int){
    private val prefs by lazy {
        context.getSharedPreferences("default", Context.MODE_PRIVATE)
    }
    operator fun getValue(thisRef: Any?, property: KProperty<*>) = prefs.getInt(name, default)
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Int) {
        prefs.edit().putInt(name, value).apply()
    }
}