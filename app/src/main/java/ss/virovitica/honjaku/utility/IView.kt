package ss.virovitica.honjaku.utility

import android.content.Context

interface IView {
    val ctx: Context
}